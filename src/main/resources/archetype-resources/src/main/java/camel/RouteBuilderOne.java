#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.AppProperties;

@Component
public class RouteBuilderOne extends RouteBuilder {

	@Autowired
	private AppProperties properties;

	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("kafka:"+properties.getKafkaUrl()).
		log("LOG RECEIVED MESSAGE").
		to("cxf:bean:cxfEndpoint").
		log("LOG SENT MESSAGE").
		end();
		
	}

}
