#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.component.cxf.CxfComponent;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.cxf.Bus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private Bus bus;
	
	@Autowired
	private AppProperties properties;

	private CamelContext context;

	@Bean(name="cxfEndpoint")
	public CxfEndpoint cxfEndpoint() {

		CxfComponent cxf = new CxfComponent(getCamelContext());
        CxfEndpoint cfxEndpoint = new CxfEndpoint("", cxf);
        cfxEndpoint.setAddress(properties.getCxfServiceUrl());
        cfxEndpoint.setCamelContext(context);
        cfxEndpoint.setDataFormat(DataFormat.PAYLOAD);
        cfxEndpoint.setBus(bus);
        
        return cfxEndpoint;
	}

	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
