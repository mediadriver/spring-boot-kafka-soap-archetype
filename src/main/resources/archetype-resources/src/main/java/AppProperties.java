#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// Kafka Endpoint parameters
	@Value("${symbol_dollar}{kafka.url}")
	private String kafkaUrl;

	// CXF parameters
	@Value("${symbol_dollar}{cxf.service.url}")
	private String cxfServiceUrl;

	public String getKafkaUrl() {
		return kafkaUrl;
	}

	public void setKafkaUrl(String kafkaUrl) {
		this.kafkaUrl = kafkaUrl;
	}

	public String getCxfServiceUrl() {
		return cxfServiceUrl;
	}

	public void setCxfServiceUrl(String cxfServiceUrl) {
		this.cxfServiceUrl = cxfServiceUrl;
	}

}
